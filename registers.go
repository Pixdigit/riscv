package riscv

type Registers struct {
	X1  int64
	X2  int64
	X3  int64
	X4  int64
	X5  int64
	X6  int64
	X7  int64
	X8  int64
	X9  int64
	X10 int64
	X11 int64
	X12 int64
	X13 int64
	X14 int64
	X15 int64
	X16 int64
	X17 int64
	X18 int64
	X19 int64
	X20 int64
	X21 int64
	X22 int64
	X23 int64
	X24 int64
	X25 int64
	X26 int64
	X27 int64
	X28 int64
	X29 int64
	X30 int64
	X31 int64
	PC  int64
}
