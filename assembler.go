package riscv

import (
	"io/ioutil"
	"strings"
)

func loadFile(path string) ([]byte, error) {
	fileBytes, err := ioutil.ReadFile(path);	if err != nil {return []byte{}, err}

	lines := strings.Split(string(fileBytes), "\n")

	_ = lines
	return []byte{}, nil
}
