package riscv

import (
	"fmt"
	"testing"
)

func TestEnv(t *testing.T) {
	test := Instruction{4294967295, R}
	test.Decode()

	bin, err := loadFile("./test.asm")
	if err != nil {
		fmt.Println(err)
	}
	for i := 0; i < len(bin); i += 4 {
		fmt.Printf("%b ", bin[i])
		fmt.Printf("%b ", bin[i+1])
		fmt.Printf("%b ", bin[i+2])
		fmt.Printf("%b ", bin[i+3])
		fmt.Println()
	}
}
