package riscv

import "fmt"

type instructionType int

type Instruction struct {
	bytes  uint32
	layout instructionType
}

const (
	R = instructionType(iota)
	I
	S
	B
	U
	J
	opcodeMask = uint32(0x0000003F) // == 0b0000000000000000000000000111111
	rdMask     = uint32(0x000007C0) // == 0b0000000000000000000011111000000
	funct3Mask = uint32(0x00003800) // == 0b0000000000000000011100000000000
	funct7Mask = uint32(0x7F000000) // == 0b1111111000000000000000000000000
	rs1Mask    = uint32(0x0007C000) // == 0b0000000000001111100000000000000
	rs2Mask    = uint32(0x00F80000) // == 0b0000000111110000000000000000000
	iImmMask   = uint32(0x7FF80000) // == 0b1111111111110000000000000000000
	sImmMask   = uint32(0x7F0007C0) // == 0b1111111000000000000011111000000
	uImmMask   = uint32(0x7FFFF800) // == 0b1111111111111111111100000000000
)

func (i Instruction) Decode() {
	opcode := uint(i.bytes & opcodeMask)
	rd := uint((i.bytes & rdMask) >> 6)
	funct3 := uint((i.bytes & funct3Mask) >> 11)
	funct7 := uint((i.bytes & funct7Mask) >> 24)
	rs1 := uint((i.bytes & rs1Mask) >> 14)
	rs2 := uint((i.bytes & rs2Mask) >> 19)
	imm := 0
	//TODO: Immediate values
	switch i.layout {
	case I:
		imm = int((i.bytes & iImmMask) >> 20)
	case S:
		imm = int(i.bytes & sImmMask)
	case B:
	case U:
	case J:
	}
	fmt.Println(opcode, rd, funct3, funct7, rs1, rs2, imm)
}
